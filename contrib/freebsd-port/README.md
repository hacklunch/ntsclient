
# TODO

* ntsclient.toml does not get installed?! make stage fails. Doesn't the
  portsystem do make install in the upstream src? It does seem to do go build
  in its own way, but eh...
* config file should not be in PLIST_FILES because it would be wiped on
  uninstall. Should install a sample? Instead?
* there is a patch in files/ which adds DISTDIR which ports seem to need. move
  it "upstream" perhaps!
* much more probably

# Updating

* get the SHA1 for the latest version and stuff into the Makefile's DISTVERSION
  and GL_COMMIT
* run `make makesum`
* ...?
